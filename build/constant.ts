/*
 * @Author: DZL
 * @Date: 2023-07-10 11:47:45
 * @LastEditors: DZL
 * @LastEditTime: 2023-07-31 10:50:57
 * @Description:
 */
/**
 * @name Config
 * @description 项目配置
 */

// 应用名
export const APP_TITLE = 'HaoTabs';

// prefix
export const API_PREFIX = '/api';

// serve
export const API_BASE_URL = '/api';
export const API_TARGET_URL = 'http://localhost:3000';

// mock
export const MOCK_API_BASE_URL = '/mock/api';
export const MOCK_API_TARGET_URL = 'http://localhost:3000';

// version
export const HAOTAB_VERSION = 'v1.0.0';
