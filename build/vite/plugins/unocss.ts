/**
 * @name ConfigUnocssPlugin
 * @description 监听配置文件修改自动重启Vite
 */

// Unocss
import Unocss from 'unocss/vite';
import { presetScrollbar } from 'unocss-preset-scrollbar';
import transformerDirectives from '@unocss/transformer-directives';

export const ConfigUnocssPlugin = () => {
  return Unocss({
    presets: [presetScrollbar()],
    // @ts-ignore
    transformers: [transformerDirectives()],
  });
};
