/* eslint-disable */
/* prettier-ignore */
// @ts-nocheck
// Generated by unplugin-vue-components
// Read more: https://github.com/vuejs/core/pull/3399
export {}

declare module 'vue' {
  export interface GlobalComponents {
    About: typeof import('./../src/components/GlobalHeader/about.vue')['default'];
    BgComp: typeof import('./../src/components/BgComp/index.vue')['default'];
    BookMark: typeof import('./../src/components/BookMark/index.vue')['default'];
    Calendar: typeof import('./../src/components/GridLayout/Calendar.vue')['default'];
    CalendarComp: typeof import('./../src/components/CalendarComp/index.vue')['default'];
    ChatAi: typeof import('./../src/components/GridLayout/ChatAi.vue')['default'];
    ChatComp: typeof import('./../src/components/ChatComp/index.vue')['default'];
    Clock: typeof import('./../src/components/GridLayout/common/Clock.vue')['default'];
    ComponentStore: typeof import('./../src/components/ComponentStore/index.vue')['default'];
    Controller: typeof import('./../src/components/wallpaperStoreComponent/Controller.vue')['default'];
    ControlPanel: typeof import('./../src/components/ControlPanel/index.vue')['default'];
    CustomLink: typeof import('./../src/components/IconComp/CustomLink.vue')['default'];
    ElButton: typeof import('element-plus/es')['ElButton'];
    ElCarousel: typeof import('element-plus/es')['ElCarousel'];
    ElCarouselItem: typeof import('element-plus/es')['ElCarouselItem'];
    ElColorPicker: typeof import('element-plus/es')['ElColorPicker'];
    ElConfigProvider: typeof import('element-plus/es')['ElConfigProvider'];
    ElDropdown: typeof import('element-plus/es')['ElDropdown'];
    ElDropdownItem: typeof import('element-plus/es')['ElDropdownItem'];
    ElDropdownMenu: typeof import('element-plus/es')['ElDropdownMenu'];
    ElEmpty: typeof import('element-plus/es')['ElEmpty'];
    ElForm: typeof import('element-plus/es')['ElForm'];
    ElFormItem: typeof import('element-plus/es')['ElFormItem'];
    ElImage: typeof import('element-plus/es')['ElImage'];
    ElInput: typeof import('element-plus/es')['ElInput'];
    ElOption: typeof import('element-plus/es')['ElOption'];
    ElPopconfirm: typeof import('element-plus/es')['ElPopconfirm'];
    ElScrollbar: typeof import('element-plus/es')['ElScrollbar'];
    ElSelect: typeof import('element-plus/es')['ElSelect'];
    ElSlider: typeof import('element-plus/es')['ElSlider'];
    ElSwitch: typeof import('element-plus/es')['ElSwitch'];
    ElUpload: typeof import('element-plus/es')['ElUpload'];
    Fly: typeof import('./../src/components/IconComp/Fly.vue')['default'];
    GlobalDock: typeof import('./../src/components/GlobalDock/index.vue')['default'];
    GlobalHeader: typeof import('./../src/components/GlobalHeader/index.vue')['default'];
    GlobalIcon: typeof import('./../src/components/GlobalIcon/index.vue')['default'];
    GlobalPopupLayout: typeof import('./../src/components/GlobalPopupLayout/index.vue')['default'];
    GlobalSearch: typeof import('./../src/components/GlobalSearch/index.vue')['default'];
    GlobalWallpaper: typeof import('./../src/components/GlobalWallpaper/index.vue')['default'];
    GridComponent: typeof import('./../src/components/GridComponent/index.vue')['default'];
    GridItem: typeof import('./../src/components/GridComponent/GridItem.vue')['default'];
    GridLayout: typeof import('./../src/components/GridLayout/index.vue')['default'];
    HotSearch: typeof import('./../src/components/GridLayout/HotSearch.vue')['default'];
    HotSearchComp: typeof import('./../src/components/HotSearchComp/index.vue')['default'];
    Icon: typeof import('./../src/components/GlobalHeader/icon.vue')['default'];
    IconComp: typeof import('./../src/components/IconComp/index.vue')['default'];
    LoadingComp: typeof import('./../src/components/LoadingComp/index.vue')['default'];
    LoginComponent: typeof import('./../src/components/LoginComponent/index.vue')['default'];
    OfficialWallpapers: typeof import('./../src/components/wallpaperStoreComponent/official-wallpapers.vue')['default'];
    RightDrawerComp: typeof import('./../src/components/RightDrawerComp/index.vue')['default'];
    RouterLink: typeof import('vue-router')['RouterLink'];
    RouterView: typeof import('vue-router')['RouterView'];
    ScanLogin: typeof import('./../src/components/LoginComponent/ScanLogin.vue')['default'];
    Search: typeof import('./../src/components/GlobalHeader/search.vue')['default'];
    SearchContent: typeof import('./../src/components/GlobalSearch/SearchContent.vue')['default'];
    SearchEng: typeof import('./../src/components/GlobalSearch/SearchEng.vue')['default'];
    SearchStore: typeof import('./../src/components/GlobalSearch/SearchStore.vue')['default'];
    Setting: typeof import('./../src/components/Setting/index.vue')['default'];
    Shortcuts: typeof import('./../src/components/GlobalHeader/shortcuts.vue')['default'];
    SolidColorWallpapers: typeof import('./../src/components/wallpaperStoreComponent/solid-color-wallpapers.vue')['default'];
    SvgIcon: typeof import('./../src/components/SvgIcon/index.vue')['default'];
    User: typeof import('./../src/components/GlobalHeader/user.vue')['default'];
    VideoWallpapers: typeof import('./../src/components/wallpaperStoreComponent/video-wallpapers.vue')['default'];
    Wallpaper: typeof import('./../src/components/GlobalHeader/wallpaper.vue')['default'];
    WallpaperStoreComponent: typeof import('./../src/components/wallpaperStoreComponent/index.vue')['default'];
    Weather: typeof import('./../src/components/GridLayout/Weather.vue')['default'];
    WeatherComp: typeof import('./../src/components/WeatherComp/index.vue')['default'];
    WeatherItem: typeof import('./../src/components/GridLayout/common/WeatherItem.vue')['default'];
    WebSite: typeof import('./../src/components/IconComp/WebSite.vue')['default'];
    WordDaily: typeof import('./../src/components/GridLayout/WordDaily.vue')['default'];
    WordDailyComp: typeof import('./../src/components/WordDailyComp/index.vue')['default'];
  }
  export interface ComponentCustomProperties {
    vInfiniteScroll: typeof import('element-plus/es')['ElInfiniteScroll'];
    vLoading: typeof import('element-plus/es')['ElLoadingDirective'];
  }
}
