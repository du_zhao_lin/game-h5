// export * from './size-ob';
// export * from './horizontal-scroll';

import { sizeOb } from './size-ob';
import { horScroll } from './horizontal-scroll';

const directives = {
  // 指令对象
  sizeOb,
  'horizontal-scroll': horScroll,
};

export default {
  install(app) {
    Object.keys(directives).forEach((key) => {
      app.directive(key, directives[key]);
    });
  },
};
