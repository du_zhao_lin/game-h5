export const horScroll = {
  mounted(el) {
    const scrollContainer = el;
    scrollContainer.addEventListener('wheel', (event) => {
      event.preventDefault();
      const scrollValue = event.deltaY;
      scrollContainer.scrollLeft += scrollValue;
    });
  },
};
