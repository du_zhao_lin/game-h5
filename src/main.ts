import { createApp } from 'vue';
import App from './App.vue';
import { router } from './router';
import piniaStore from './store';
import '/@/styles/index.less';
import '/@/styles/reset.less';
import 'uno.css';

import 'virtual:svg-icons-register';
import directives from '/@/directives';

//vue3的挂载方式
const app = createApp(App);

app.use(directives);

app.use(router);
app.use(piniaStore);

app.mount('#app');
