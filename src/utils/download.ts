import { getCurrentTime } from './format';

/**
 * 下载大文件最好还是直接使用a链接+校验
 * @param fileUrl {string}  下载的链接
 * @param fileName {string} 下载的名称
 */
export function download(fileUrl: string, fileName?: string) {
  fetch(fileUrl)
    .then((response) => response.blob())
    .then((blob) => {
      const url = URL.createObjectURL(blob);
      const currentTime = getCurrentTime();
      const link = document.createElement('a');
      link.href = url;
      document.body.appendChild(link);
      link.download = fileName || currentTime;
      link.click();
      document.body.removeChild(link);
    });
}
