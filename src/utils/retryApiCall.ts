/**
 *
 * @param apiFunction 接口函数
 * @param maxRetries 重试次数
 * @param retryDelay 重试时间
 * @returns
 */

export async function retryApiCall(apiFunction, maxRetries = 1, retryDelay = 100) {
  let retries = 0;
  while (retries < maxRetries) {
    try {
      return await apiFunction(); // 返回成功的响应
    } catch (error) {
      retries++;
      if (retries < maxRetries) {
        // 如果尚未达到最大重试次数，则等待一段时间后再次尝试
        await new Promise((resolve) => setTimeout(resolve, retryDelay));
      }
    }
  }
  throw new Error(`接口请求失败, 重试中 ${maxRetries}`);
}
